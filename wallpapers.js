
// One of ElementaryOS default Wallpapers
import elementary from './wallpapers/elementary.jpg';
import coldwarm from './wallpapers/coldwarm.jpg';

const WALLPAPERS = {
    coldwarm: {
        id:'coldwarm',
        path: coldwarm,
        theme:{
            id:'light',
            textOnWallpaper:'black'
        },
        bg: {
            background: `url(${coldwarm}) no-repeat center center`,
            backgroundSize: 'cover'
        }
    },
    elementary: {
        id:'elementary',
        path: elementary,
        theme:{
            id:'dark',
            textOnWallpaper:'white'
        },
        bg: {
            background: `url(${elementary}) no-repeat center center`,
            backgroundSize: 'cover'

        }
    }
};

function isWallpaper(name) {
    if(Object.keys(WALLPAPERS).indexOf(name)===-1) {
        return false;
    } else {
        return true;
    }
}

let enforceWallpaper = "elementary";
function defaultWallpaper() {
    if(!!enforceWallpaper) return enforceWallpaper;
    return Object.keys(WALLPAPERS)[0];
}

export {
    WALLPAPERS,
    isWallpaper,
    defaultWallpaper
}
